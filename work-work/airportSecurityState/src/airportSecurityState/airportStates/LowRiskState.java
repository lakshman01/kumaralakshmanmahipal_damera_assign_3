package airportSecurityState.airportStates;

import airportSecurityState.util.Context;
import airportSecurityState.util.DataProcessing;
import airportSecurityState.util.Helper;
import airportSecurityState.util.MyLogger;
import airportSecurityState.util.MyLogger.DebugLevel;

public class LowRiskState implements AirportStateI {
	Context context;
	private DataProcessing dp;
	private Helper h;

	public LowRiskState(Context context, DataProcessing dp, Helper helper) {
		MyLogger.writeMessage("Constructor in LowRiskState Class Called", DebugLevel.CONSTRUCTOR);  //using mylogger to debug
		this.context = context;
		this.dp = dp;
		h = helper;
	}

	public void tightenOrLoosenSecurity(String s) {
		h.splitStrings(s);       //utilising methods in helper class
		int ret = h.calcAvgTrafficPerDay();
		int ret1 = h.calcAvgProhItemsPerDay();
		if ((ret >= 4 && ret < 8) || ret1 >= 1 && ret1 < 2) {
			MyLogger.writeMessage("State Change LowRiskState -> ModerateRiskState", DebugLevel.IN_RUN);
			context.setState(context.getModRiskState());   
			String output = "2 " + "3 " + "5 " + "8 " + "9";
			dp.writeToFile1(output);
			MyLogger.writeMessage("Output String "+output+" Written To File in ModerateRiskState",DebugLevel.FROM_RESULTS );
		}

		else {
			String output = "1 " + "3 " + "5 " + "7 " + "9";
			dp.writeToFile1(output);
			MyLogger.writeMessage("Output String "+output+" Written To File in LowRiskState",DebugLevel.FROM_RESULTS );
		}
	}
}