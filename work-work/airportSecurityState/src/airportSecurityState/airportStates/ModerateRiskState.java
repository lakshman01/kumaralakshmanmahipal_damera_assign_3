package airportSecurityState.airportStates;

import airportSecurityState.util.Context;
import airportSecurityState.util.DataProcessing;
import airportSecurityState.util.Helper;
import airportSecurityState.util.MyLogger;
import airportSecurityState.util.MyLogger.DebugLevel;

public class ModerateRiskState implements AirportStateI {
	Context context;
	private DataProcessing dp;
	private Helper h;

	public ModerateRiskState(Context context, DataProcessing dp, Helper h) {
		MyLogger.writeMessage("Constructor in ModerateRiskState Class Called", DebugLevel.CONSTRUCTOR);
		this.context = context;
		this.dp = dp;
		this.h = h;
	}

	public void tightenOrLoosenSecurity(String s) {

		h.splitStrings(s);
		int ret = h.calcAvgTrafficPerDay();
		int ret1 = h.calcAvgProhItemsPerDay();
		if ((ret >= 8) || ret1 >= 2) {
			MyLogger.writeMessage("State Change ModerateRiskState -> HighRiskState", DebugLevel.IN_RUN);
			context.setState(context.getHighRiskState());
			String output = "2 " + "4 " + "6 " + "8 " + "10";
			dp.writeToFile1(output);
			MyLogger.writeMessage("Output String "+output+" Written To File in HighRiskState",DebugLevel.FROM_RESULTS );

		} else if ((ret >= 4 && ret < 8) || ret1 >= 1 && ret1 < 2) {
			String output = "2 " + "3 " + "5 " + "8 " + "9";
			dp.writeToFile1(output);
			MyLogger.writeMessage("Output String "+output+" Written To File in ModerateRiskState",DebugLevel.FROM_RESULTS );
		} else if ((ret >= 0 && ret < 4) || (ret1 >= 0 && ret1 < 1)) {
			MyLogger.writeMessage("State Change ModerateRiskState -> LowRiskState", DebugLevel.IN_RUN);
			context.setState(context.getLowRiskState());
			String output = "1 " + "3 " + "5 " + "7 " + "9";
			dp.writeToFile1(output);
			MyLogger.writeMessage("Output String "+output+" Written To File in LowRiskState",DebugLevel.FROM_RESULTS );
		}
	}
}
