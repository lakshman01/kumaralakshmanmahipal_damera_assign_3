package airportSecurityState.airportStates;

import airportSecurityState.util.Context;
import airportSecurityState.util.DataProcessing;
import airportSecurityState.util.Helper;
import airportSecurityState.util.MyLogger;
import airportSecurityState.util.MyLogger.DebugLevel;

public class HighRiskState implements AirportStateI {
	Context context;
	private DataProcessing dp;
	private Helper h;

	public HighRiskState(Context context, DataProcessing dp, Helper h) {
		MyLogger.writeMessage("Constructor in HighRiskState Class Called", DebugLevel.CONSTRUCTOR);
		this.context = context;
		this.dp = dp;
		this.h = h;

	}

	public void tightenOrLoosenSecurity(String s) {
		h.splitStrings(s);         //using helper class methods to process the string
		int ret = h.calcAvgTrafficPerDay();
		int ret1 = h.calcAvgProhItemsPerDay();

		if ((ret >= 4 && ret < 8) || ret1 >= 1 && ret1 < 2) {   //checking for possible states and performing particular action
			MyLogger.writeMessage("State Change HighRiskState -> ModerateRiskState", DebugLevel.IN_RUN);
			context.setState(context.getModRiskState());
			String output = "2 " + "3 " + "5 " + "8 " + "9";
			dp.writeToFile1(output);         //writing output to file
			MyLogger.writeMessage("Output String "+output+" Written To File in ModerateRiskState",DebugLevel.FROM_RESULTS );
		} else {
			if ((ret >= 8) || ret1 >= 2) {
				String output = "2 " + "4 " + "6 " + "8 " + "10";
				dp.writeToFile1(output);
				MyLogger.writeMessage("Output String "+output+" Written To File in HighRiskState",DebugLevel.FROM_RESULTS );
			}

		}
	}
}
