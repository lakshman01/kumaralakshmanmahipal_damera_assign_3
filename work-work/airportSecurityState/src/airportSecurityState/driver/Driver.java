package airportSecurityState.driver;

import airportSecurityState.util.Context;
import airportSecurityState.util.DataProcessing;
import airportSecurityState.util.Helper;
import airportSecurityState.util.MyLogger;
import airportSecurityState.util.MyLogger.DebugLevel;

public class Driver {
	public static boolean flag = false;

	public static void main(String[] args) {
		String s = null;
		try {
			DataProcessing dp = new DataProcessing(args[0], args[1]);
			String str = args[2];
			Driver d = new Driver();
			d.argumentsCheck(args);
			d.validityCheck(str);
			Helper h = new Helper();
			Context c = new Context(dp, h);

			while ((s = dp.Return()) != null) {
				c.tightenOrLoosenSecurity(s);
			}
		} catch (NumberFormatException e) {
			System.err.println("Given Input is not a Integer" + e);
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Exception Found" + e);
			System.exit(1);
		}
	}
	public void argumentsCheck(String[] args) {
		if (args.length != 3 ) {
			System.err.println("Arguments Parameters must be exactly 3. You Entered Only "+args.length+" Parameters");
			System.exit(1);
		}
		if (args[0] != null && args.length == 1) {
			flag = true;
		}
	}

	public void validityCheck(String str) {
		String string;
		string = str;
		try {
			int integer = Integer.parseInt(string);
			if (integer >= 0 && integer <= 4) {
				MyLogger.setDebugValue(integer);
			}
			else {
				System.err.println("Debug values in Range 0-4 only allowed. You Entered "+integer);
				System.exit(1);
			}
		} catch (NumberFormatException e) {
			System.err.println("Given Input is not a Integer   " + e);
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Exception Found" + e);
			System.exit(1);
		}
	}
}