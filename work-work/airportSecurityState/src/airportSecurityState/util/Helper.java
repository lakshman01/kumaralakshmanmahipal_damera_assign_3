package airportSecurityState.util;

import java.util.ArrayList;
import airportSecurityState.util.MyLogger.DebugLevel;

public class Helper {
	private int totNumTravel = 0; // total number of travellers
	private int totNumDays = 0; // total number of days
	private int avgTrafficPerDay = 0;
	private int avgProhItemPerDay = 0;
	private int count = 0, prItems = 0;
	ArrayList<String> noItemList = new ArrayList<String>();

	public Helper() {
		MyLogger.writeMessage("Constructor in Helper Class Called", DebugLevel.CONSTRUCTOR);
		setNoItemList("Gun"); // setting arraylist to the prohibited items
		setNoItemList("NailCutter");
		setNoItemList("Blade");
		setNoItemList("Knife");
	}

	public ArrayList<String> getNoItemList() {
		return noItemList;
	}

	public int getAvgTrafficPerDay() {
		return avgTrafficPerDay;
	}

	public void setAvgTrafficPerDay(int avgTrafficPerDay) {
		this.avgTrafficPerDay = avgTrafficPerDay;
	}

	public int getAvgProhItemPerDay() {
		return avgProhItemPerDay;
	}

	public void setAvgProhItemPerDay(int avgProhItemPerDay) {
		this.avgProhItemPerDay = avgProhItemPerDay;
	}

	public void setNoItemList(String s) {
		noItemList.add(s);
	}

	public int getTotNumTravel() {
		return totNumTravel;
	}

	public void setTotNumTravel(int totNumTravel) {
		this.totNumTravel = totNumTravel;
	}

	public int getTotNumDays() {
		return totNumDays;
	}

	public void setTotNumDays(int totNumDays) {
		this.totNumDays = totNumDays;
	}

	public void splitStrings(String s) { // method to split the given string
		count = count + 1;
		setTotNumTravel(count); // no of travellers so far
		try {
			String[] s1 = s.split(";");
			String[] s2 = s1[0].split(":");
			int st = Integer.parseInt(s2[1]); // no of total days so far
			setTotNumDays(Integer.parseInt(s2[1]));
			String[] s3 = s1[3].split(":");
			for (String str : getNoItemList()) {
				if (str.matches(s3[1])) {
					prItems += 1; // no of prohibited items so far
				}
			}
			MyLogger.writeMessage(
					"Fetched Line & Traveller: " + count + " Proh Items & No of Days So Far: " + prItems + "  " + st,
					DebugLevel.IN_HELPER);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public int getPrItems() {
		return prItems;
	}

	public void setPrItems(int prItems) {
		this.prItems = prItems;
	}

	public int calcAvgTrafficPerDay() { // method to calculate average traffic per day
		int avgTraffic;
		avgTraffic = getTotNumTravel() / getTotNumDays();
		setAvgTrafficPerDay(avgTraffic);
		return avgTraffic;
	}

	public int calcAvgProhItemsPerDay() { // method to calculate average prohibited items per day
		int avgProhItem;
		avgProhItem = getPrItems() / getTotNumDays();
		setAvgProhItemPerDay(avgProhItem);
		return avgProhItem;
	}

	public String toString() {
		return "Number of Travellers= " + totNumTravel;
	}

}
