package airportSecurityState.util;

import airportSecurityState.airportStates.AirportStateI;
import airportSecurityState.airportStates.HighRiskState;
import airportSecurityState.airportStates.LowRiskState;
import airportSecurityState.airportStates.ModerateRiskState;
import airportSecurityState.util.MyLogger.DebugLevel;

public class Context {
	AirportStateI lowRiskState;
	AirportStateI modRiskState;
	AirportStateI highRiskState;
	AirportStateI curState ;

	public Context(DataProcessing dp,Helper h) {
		MyLogger.writeMessage("Constructor in Context Class Called", DebugLevel.CONSTRUCTOR);
		lowRiskState = new LowRiskState(this, dp,h);
		modRiskState = new ModerateRiskState(this,dp,h);
		highRiskState = new HighRiskState(this,dp,h);
		this.curState = lowRiskState;
	}
 
	public AirportStateI getLowRiskState() {
		return lowRiskState;
	}

	public AirportStateI getModRiskState() {
		return modRiskState;
	}

	public AirportStateI getHighRiskState() {
		return highRiskState;
	}

	public void setState(AirportStateI state) {
		this.curState = state;
	}

	public void tightenOrLoosenSecurity(String s) {
		this.curState.tightenOrLoosenSecurity(s);
	}

}
