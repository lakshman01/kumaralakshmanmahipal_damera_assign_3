package airportSecurityState.util;

public class MyLogger {

	/*
	 * DEBUG_VALUE=4 [Printing to stdout everytime a constructor is called]
	 * DEBUG_VALUE=3 [Printing to stdout everytime the state is changed] 
	 * DEBUG_VALUE=2 [Printing to stdout everytime the number of lines fetched,Total Num of Travellers,Prohibited Items and Total Num Of Days Thus far] 
	 * DEBUG_VALUE=1 [Printing to stdout everytime a line is written to file along with string being written and the current state of the airport ] 
	 * DEBUG_VALUE=0 [Printing no output from the application to stdout. It is ok to write to the output file though" ]
	 */

	public static enum DebugLevel {
		RELEASE, FROM_RESULTS, IN_HELPER, IN_RUN, CONSTRUCTOR
	};

	private static DebugLevel debugLevel;

	public static void setDebugValue(int levelIn) {
		switch (levelIn) {
		case 4:
			debugLevel = DebugLevel.CONSTRUCTOR;
			break;
		case 3:
			debugLevel = DebugLevel.IN_RUN;
			break;
		case 2:
			debugLevel = DebugLevel.IN_HELPER;
			break;
		case 1:
			debugLevel = DebugLevel.FROM_RESULTS;
			break;
		case 0:
			debugLevel = DebugLevel.RELEASE;
			break;
		}
	}

	public static void setDebugValue(DebugLevel levelIn) {
		debugLevel = levelIn;
	}

	// @return None
	public static void writeMessage(String message, DebugLevel levelIn) {
		if (levelIn == debugLevel)
			System.out.println(message);
	}

	/**
	 * @return String
	 */
	public String toString() {
		return "Debug Level is " + debugLevel;
	}
}