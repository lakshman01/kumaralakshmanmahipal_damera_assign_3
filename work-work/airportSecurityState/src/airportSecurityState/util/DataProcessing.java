package airportSecurityState.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import airportSecurityState.util.MyLogger.DebugLevel;

public class DataProcessing {

	private BufferedReader breader;
	private BufferedWriter bwriter;
	private String IfileName;
	private String OfileName;

	public DataProcessing(String inputfname, String outputfname) // Constructor to accept command line arguments from
																	// Driver.java
	{
		this.IfileName = inputfname;
		this.OfileName = outputfname;
		try {
			FileReader fr = new FileReader(IfileName);
			breader = new BufferedReader(fr);
			MyLogger.writeMessage("Parameterised Constructor in DataProcessing Class Called", DebugLevel.CONSTRUCTOR);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileWriter fw = new FileWriter(OfileName);
			bwriter = new BufferedWriter(fw);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String Return() throws Exception { // method for reading line by line from the inputfile
		String input = null;
		try {
			input = breader.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return input;
	}

	public void writeToFile1(String s) { // method for writing to file
		try {
			bwriter.write(s + "\n");
			bwriter.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}